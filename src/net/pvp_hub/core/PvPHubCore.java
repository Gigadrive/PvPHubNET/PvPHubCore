package net.pvp_hub.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.md_5.bungee.api.ChatColor;
import net.pvp_hub.core.api.GameState;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.core.cmd.Chips;
import net.pvp_hub.core.cmd.Coins;
import net.pvp_hub.core.cmd.GameMode;
import net.pvp_hub.core.cmd.GiveAchievement;
import net.pvp_hub.core.cmd.Money;
import net.pvp_hub.core.cmd.Nick;
import net.pvp_hub.core.cmd.Plugins;
import net.pvp_hub.core.cmd.RNick;
import net.pvp_hub.core.cmd.RankList;
import net.pvp_hub.core.cmd.Register;
import net.pvp_hub.core.cmd.SetRank;
import net.pvp_hub.core.cmd.Skull;
import net.pvp_hub.core.cmd.Vanish;
import net.pvp_hub.core.listener.AsyncPlayerChatListener;
import net.pvp_hub.core.listener.AsyncPlayerReceiveTagListener;
import net.pvp_hub.core.listener.BlockPlaceListener;
import net.pvp_hub.core.listener.PlayerJoinListener;
import net.pvp_hub.core.listener.PlayerQuitListener;
import net.pvp_hub.core.listener.ServerPingListener;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.MySQLData;
import tk.theakio.sql.QueryResult;
import tk.theakio.sql.SQLHandler;

public class PvPHubCore extends JavaPlugin implements Listener {
	
	@SuppressWarnings("unused")
	private QueryResult qr = null;
	public String main = "main";
	
	public static File file = new File("plugins/PvPHubCore", "userdata.yml");
	public static FileConfiguration user_cfg = YamlConfiguration.loadConfiguration(file);
	
	public static boolean running = true;
	public static int PID = 0;
	public static long interval = 300L;
	public static int currentLine = 0;
	
	public static Map<String, String> nicked = new HashMap<String, String>();
	public static ArrayList<Player> vanished = new ArrayList<Player>();
	
	private PvPHubCore instance = this;
	public PvPHubCore getInstance(){
		return this.instance;
	}
	
	private static GameState state = GameState.UNDEFINED;
	
	//public static ScoreboardManager manager = Bukkit.getScoreboardManager();
	public Scoreboard colours = getServer().getScoreboardManager().getMainScoreboard();
	public static Team owner = null;
	public static Team admin = null;
	public static Team dev = null;
	public static Team mod = null;
	public static Team team = null;
	public static Team build = null;
	public static Team beta = null;
	public static Team vip = null;
	public static Team premium = null;
	public static Team user = null;
	
	@SuppressWarnings("deprecation")
	public void onEnable(){
		this.createTeams();
		
		// /coins
		getCommand("coins").setExecutor(new Coins(this));
			
		// /chips
		getCommand("chips").setExecutor(new Chips(this));
		
		// /money
		getCommand("money").setExecutor(new Money(this));
		
		// /skull
		getCommand("skull").setExecutor(new Skull(this));
		
		// /register
		getCommand("register").setExecutor(new Register(this));
		
		// /plugins
		getCommand("plugins").setExecutor(new Plugins(this));
		
		// /nick
		getCommand("nick").setExecutor(new Nick(this));
				
		// /rnick
		getCommand("rnick").setExecutor(new RNick(this));
		
		// /realname
		getCommand("realname").setExecutor(new Nick(this));
		
		// /giveachievement
		getCommand("giveachievement").setExecutor(new GiveAchievement(this));
		
		// /ranklist
		getCommand("ranklist").setExecutor(new RankList(this));
		
		// /rank
		getCommand("rank").setExecutor(new SetRank(this));
		
		// /vanish
		getCommand("vanish").setExecutor(new Vanish(this));
		
		// /gamemode
		getCommand("gamemode").setExecutor(new GameMode(this));
		
		Bukkit.getPluginManager().registerEvents(new AsyncPlayerChatListener(this), this);
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(this), this);
		Bukkit.getPluginManager().registerEvents(new AsyncPlayerReceiveTagListener(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockPlaceListener(this), this);
		//Bukkit.getPluginManager().registerEvents(new TabCompleteListener(this), this);
		Bukkit.getPluginManager().registerEvents(new ServerPingListener(this), this);
		
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		
		SQLHandler Handler = HandlerStorage.addHandler(main, new SQLHandler());
		Handler.setMySQLOptions(MySQLData.mysql_u_host, MySQLData.mysql_u_port, MySQLData.mysql_u_user, MySQLData.mysql_u_pass, MySQLData.mysql_u_database);
		try {
			Handler.openConnection();
		} catch (Exception e) {
			System.err.println("[MySQL API von Akio Zeugs] Putt Putt!");
			e.printStackTrace();
		}
		
		for(Player all : Bukkit.getOnlinePlayers()){
			all.kickPlayer("�cReloading..");
		}
		
		PID = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable()
	    {
	      public void run()
	      {
	        try
	        {
	        	PvPHubCore.broadcastMessages("plugins/PvPHubCore/broadcastMessages.txt");
	        }
	        catch (IOException e)
	        {
	          System.err.println("[ServerMain] Couldn't load Broadcaster Extension.");
	          System.err.println("[ServerMain] Stopping Broadcaster Extension.");
	          Bukkit.getScheduler().cancelTask(PvPHubCore.PID);
	          PvPHubCore.running = false;
	        }
	      }
	    }, 200L, interval * 20L);
	}
	
	public void onDisable() {
		owner.unregister();
		admin.unregister();
		dev.unregister();
		mod.unregister();
		team.unregister();
		build.unregister();
		beta.unregister();
		vip.unregister();
		premium.unregister();
		user.unregister();
	}
	
	public static void setGameState(GameState state){
		PvPHubCore.state = state;
	}
	
	public static GameState getGameState(){
		return PvPHubCore.state;
	}
	
	public void createTeams() {
		owner = colours.registerNewTeam("owner");
		owner.setPrefix(ChatColor.RED.toString());
	    
		admin = colours.registerNewTeam("admin");
	    admin.setPrefix(ChatColor.RED.toString());
	    
	    dev = colours.registerNewTeam("dev");
	    dev.setPrefix(ChatColor.BLUE.toString());
	    
	    mod = colours.registerNewTeam("mod");
	    mod.setPrefix(ChatColor.YELLOW.toString());
	    
	    team = colours.registerNewTeam("team");
	    team.setPrefix(ChatColor.GREEN.toString());
	    
	    build = colours.registerNewTeam("build");
	    build.setPrefix(ChatColor.DARK_AQUA.toString());
	    
	    beta = colours.registerNewTeam("beta");
	    beta.setPrefix(ChatColor.AQUA.toString());
	    
	    vip = colours.registerNewTeam("vip");
	    vip.setPrefix(ChatColor.DARK_PURPLE.toString());
	    
	    premium = colours.registerNewTeam("premium");
	    premium.setPrefix(ChatColor.GOLD.toString());
	    
	    user = colours.registerNewTeam("user");
	    user.setPrefix(ChatColor.GRAY.toString());
	}
	
	public void joinTeam(Team team, Player p) {
	    team.addPlayer(p);
	}
	
	public static void leaveTeam(Player p){
		if(owner.getPlayers().contains(p)){
			owner.removePlayer(p);
			return;
		} else if(admin.getPlayers().contains(p)){
			admin.removePlayer(p);
			return;
		} else if(dev.getPlayers().contains(p)){
			dev.removePlayer(p);
			return;
		} else if(mod.getPlayers().contains(p)){
			mod.removePlayer(p);
			return;
		} else if(team.getPlayers().contains(p)){
			team.removePlayer(p);
			return;
		} else if(build.getPlayers().contains(p)){
			build.removePlayer(p);
			return;
		} else if(beta.getPlayers().contains(p)){
			beta.removePlayer(p);
			return;
		} else if(vip.getPlayers().contains(p)){
			vip.removePlayer(p);
			return;
		} else if(premium.getPlayers().contains(p)){
			premium.removePlayer(p);
			return;
		} else if(user.getPlayers().contains(p)){
			user.removePlayer(p);
			return;
		} else {
			return;
		}
	}
	
	@SuppressWarnings("resource")
	public static void broadcastMessages(String path) throws IOException {
	    FileInputStream stream = new FileInputStream(path);
	    
	    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
	    for (int i = 0; i < currentLine; i++) {
	      reader.readLine();
	    }
	    String line = reader.readLine();
	    
	    line = line.replaceAll("&a", "�a");
	    line = line.replaceAll("&c", "�c");
	    line = line.replaceAll("&4", "�4");
	    line = line.replaceAll("&6", "�6");
	    line = line.replaceAll("&l", "�l");
	    line = line.replaceAll("&o", "�o");
	    line = line.replaceAll("&9", "�9");
	    line = line.replaceAll("&0", "�0");
	    line = line.replaceAll("&8", "�8");
	    line = line.replaceAll("&9", "�9");
	    line = line.replaceAll("&d", "�d");
	    line = line.replaceAll("&e", "�e");
	    line = line.replaceAll("&3", "�3");
	    line = line.replaceAll("&b", "�b");
	    line = line.replaceAll("&7", "�7");
	    line = line.replaceAll("&1", "�1");
	    line = line.replaceAll("&2", "�2");
	    line = line.replaceAll("&5", "�5");
	    line = line.replaceAll("&f", "�f");
	    line = line.replaceAll("&r", "�r");
	 
	    Bukkit.getServer().broadcastMessage(PluginMeta.prefixBroadcaster + line);
	    
	    LineNumberReader lnr = new LineNumberReader(new FileReader(new File(path)));
	    lnr.skip(9223372036854775807L);
	    int lastLine = lnr.getLineNumber();
	    if (currentLine + 1 == lastLine + 1) {
	      currentLine = 0;
	    } else {
	      currentLine += 1;
	    }
	}
	
	public static void createNickLog(String player, String nick) throws Exception{
		HandlerStorage.getHandler("main").execute("INSERT INTO nickLogger (`who` ,`nick`)VALUES ('" + player + "', '" + nick + "');");
	}
	
	public static void createCmdLog(String player, int x, int y, int z) throws Exception{
		HandlerStorage.getHandler("main").execute("INSERT INTO cmdLogger (`who`, `x`, `y`, `z`)VALUES ('" + player + "', " + x + ", " + y + ", " + z + ");");
	}
	
	@EventHandler
    public void onNameTag(AsyncPlayerReceiveNameTagEvent event) {
    	//event.setTag(event.getNamedPlayer().getPlayerListName());
    }

}
