package net.pvp_hub.core.achievement;

import net.pvp_hub.core.api.PlayerUtilities;

import org.bukkit.entity.Player;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;

public class AchievementData {

	public static String getName(int aid) throws Exception {
		String name = "";
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM achievements WHERE id='" + aid + "'");
		qr.rs.last();
		
		name = qr.rs.getString("name");
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return name;
	}
	
	public static String getDescription(int aid) throws Exception {
		String description = "";
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM achievements WHERE id='" + aid + "'");
		qr.rs.last();
		
		description = qr.rs.getString("description");
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return description;
	}
	
	public static String getAchievers(int aid) throws Exception {
		String achieved_by = "";
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM achievements WHERE id='" + aid + "'");
		qr.rs.last();
		
		achieved_by = qr.rs.getString("achieved_by");
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return achieved_by;
	}
	
	public static boolean hasAchieved(int aid, Player p) throws Exception {
		String pidstring = PlayerUtilities.getPvPHubId(p) + "";
		
		boolean b = false;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM achievements WHERE id='" + aid + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			String achievers = AchievementData.getAchievers(aid);
			
			if(achievers.contains(pidstring)){
				b = true;
			} else {
				b = false;
			}
		}
		
		return b;
	}
	
}
