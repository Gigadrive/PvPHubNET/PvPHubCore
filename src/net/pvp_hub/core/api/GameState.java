package net.pvp_hub.core.api;

public enum GameState {

	UNDEFINED(0, "�8�lUndefined"),
	LOBBY(1, "�a�lLobby"),
	INGAME(2, "�4�lIngame"),
	ALWAYS_JOIN(3, "�a�lJoinable"),
	WARMUP(4, "�7�lWarmup"),
	ENDING(5, "�c�lEnding");
	
	private int id;
	private String name;
	
	GameState(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int getID(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public static GameState fromID(int id){
		switch(id) {
			case 0: return UNDEFINED;
			case 1: return LOBBY;
			case 2: return INGAME;
			case 3: return ALWAYS_JOIN;
			case 4: return WARMUP;
			case 5: return ENDING;
		}
		return UNDEFINED;
	}
	
}
