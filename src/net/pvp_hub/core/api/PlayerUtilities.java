package net.pvp_hub.core.api;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.UUID;

import net.minecraft.server.v1_7_R4.ChatSerializer;
import net.minecraft.server.v1_7_R4.IChatBaseComponent;
import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.achievement.AchievementData;
import net.pvp_hub.core.language.Language;
import net.pvp_hub.core.player.Rank;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.Team;
import org.spigotmc.ProtocolInjector.PacketTabHeader;
import org.spigotmc.ProtocolInjector.PacketTitle;
import org.spigotmc.ProtocolInjector.PacketTitle.Action;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.InvalidDatabaseTypeException;
import tk.theakio.sql.QueryResult;

public class PlayerUtilities {
	
	public static String cutDisplayName(String name) {
		if(name.length() > 14) {
			return name.substring(0, 14);
		}
		return name;
	}
	
	public static void clearInventory(Player p){
		p.getInventory().clear();
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
	}
	
	public static String getBuyers(int id) throws Exception {
		String buyers = "";
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM shopItems WHERE id=" + id + "");
		qr.rs.last();
		if(qr.rs.getRow() != 0){
			buyers = qr.rs.getString("bought_by");
		}
		
		return buyers;
	}
	
	public static Integer getPvPHubId(Player p) throws Exception {
		UUID uuid = p.getUniqueId();
		int id = 1;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			id = qr.rs.getInt("id");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return id;
	}
	
	public static boolean hasBoughtShopItem(Player p, int id) throws Exception {
		boolean b = false;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM shopItems WHERE id=" + id + "");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			if(qr.rs.getString("bought_by").contains("," + getPvPHubId(p))){
				b = true;
			} else {
				b = false;
			}
		}
		
		return b;
	}
	
	public static void buyShopItem(Player p, int id) throws Exception {
		HandlerStorage.getHandler("main").execute("UPDATE `shopItems` SET `bought_by` = '" + getBuyers(id) + "," + getPvPHubId(p) + "' WHERE id=" + id + ";");
	}
	
	public static int getCoinsForItem(int id) throws Exception {
		int b = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM shopItems WHERE id=" + id + "");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			b = qr.rs.getInt("price_coins");
		}
		
		return b;
	}
	
	public static int getChipsForItem(int id) throws Exception {
		int b = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM shopItems WHERE id=" + id + "");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			b = qr.rs.getInt("price_chips");
		}
		
		return b;
	}
	
	@SuppressWarnings("deprecation")
	public static boolean isOnline(String player){
		if(Bukkit.getPlayer(player) == null){
			return false;
		} else {
			return true;
		}
	}
	
	public static void removeAllFromTeams(){
		for(Team t : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()){
			t.getPlayers().clear();
			t.unregister();
		}
	}
	
	public static void updatePassword(Player p, String hash){
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `users` SET `website_pass` = '" + hash + "' WHERE uuid='" + getUUID(p.getName()) + "'");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDatabaseTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static int getCoins(Player p) throws Exception {
		int coins = 100;
		UUID user = p.getUniqueId();
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			coins = qr.rs.getInt("coins");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return coins;
	}
	
	public static String getServer(Player p) throws Exception {
		String server = "";
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			server = qr.rs.getString("server");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return server;
	}
	
	public static void addCoins(Player p, int coins) throws Exception {
		int oldCoins = getCoins(p);
		int newCoins = oldCoins + coins;
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `users` SET `coins` = " + newCoins + " WHERE uuid='" + getUUID(p.getName()) + "'");
			if(coins == 1){
				p.sendMessage(PluginMeta.prefix + "�aDir wurde �e" + coins + " �aCoin gutgeschrieben.");
			} else {
				p.sendMessage(PluginMeta.prefix + "�aDir wurden �e" + coins + " �aCoins gutgeschrieben.");
			}
		} catch (Exception e){
			p.sendMessage(PluginMeta.prefix + "�c" + "Something went wrong..");
		   	p.sendMessage(PluginMeta.prefix + "�c" + "Take a look in the console!");
		  	e.printStackTrace();
		}
	}
	
	/*public static Language getLanguage(Player p) throws Exception {
		Language lang = Language.ENGLISH;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			lang = Language.fromID(qr.rs.getInt("language"));
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return lang;
	}*/
	
	public static Language getLanguage(Player p) throws Exception {
		Object ep = getMethod("getHandle", p.getClass()).invoke(p, (Object[]) null);
		Field f = ep.getClass().getDeclaredField("locale");
		f.setAccessible(true);
		
		String language = (String) f.get(ep);
		
		return Language.fromName(language);
	}
	
	private static Method getMethod(String name, Class<?> clazz) {
		for (Method m : clazz.getDeclaredMethods()) {
			if (m.getName().equals(name))
			return m;
		}
		return null;
	}
	
	public static String getRankColor(int rank) throws Exception {
		if(rank == 0){
			return "�7";
		} else if(rank == 1){
			return "�6";
		} else if(rank == 2){
			return "�6";
		} else if(rank == 3){
			return "�5";
		} else if(rank == 4){
			return "�5";
		} else if(rank == 5){
			return "�b";
		} else if(rank == 6){
			return "�3";
		} else if(rank == 7){
			return "�a";
		} else if(rank == 8){
			return "�e";
		} else if(rank == 9){
			return "�9";
		} else if(rank == 10){
			return "�c";
		} else if(rank == 11){
			 return "�c";
		} else {
			return "�4[ERROR]";
		}
	}
	
	public static void setRank(String p, int rank) throws Exception {
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `users` SET `rank` = " + rank + " WHERE uuid='" + getUUID(p) + "'");
			if(isOnline(p)){
				Player pl = Bukkit.getPlayer(p);
				pl.kickPlayer("�aDein Rang wurde geupdated.\nDamit die Daten aktualisiert werden k�nnen\nmusst du reconnecten.");
			}
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void removeCoins(Player p, int coins) throws Exception {
		int oldCoins = getCoins(p);
		if(oldCoins < coins){
			p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
			return;
		}
		int newCoins = oldCoins - coins;
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `users` SET `coins` = " + newCoins + " WHERE uuid='" + getUUID(p.getName()) + "'");
			if(coins == 1){
				p.sendMessage(PluginMeta.prefix + "�cDir wurde �e" + coins + " �cCoin abgezogen.");
			} else {
				p.sendMessage(PluginMeta.prefix + "�cDir wurden �e" + coins + " �cCoins abgezogen.");
			}
		} catch (Exception e){
			p.sendMessage(PluginMeta.prefix + "�c" + "Something went wrong..");
		   	p.sendMessage(PluginMeta.prefix + "�c" + "Take a look in the console!");
		  	e.printStackTrace();
		}
	}
	
	public static void addChips(Player p, int chips) throws Exception {
		int oldCoins = getChips(p);
		int newCoins = oldCoins + chips;
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `users` SET `chips` = " + newCoins + " WHERE uuid='" + getUUID(p.getName()) + "'");
			if(chips == 1){
				p.sendMessage(PluginMeta.prefix + "�aDir wurde �e" + chips + " �6Chip �agutgeschrieben.");
			} else {
				p.sendMessage(PluginMeta.prefix + "�aDir wurden �e" + chips + " �6Chips �agutgeschrieben.");
			}
		} catch (Exception e){
			p.sendMessage(PluginMeta.prefix + "�c" + "Something went wrong..");
		   	p.sendMessage(PluginMeta.prefix + "�c" + "Take a look in the console!");
		  	e.printStackTrace();
		}
	}
	
	public static void removeChips(Player p, int chips) throws Exception {
		int oldCoins = getChips(p);
		if(oldCoins < chips){
			p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Chips.");
			return;
		}
		int newCoins = oldCoins - chips;
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `users` SET `chips` = " + newCoins + " WHERE uuid='" + getUUID(p.getName()) + "'");
			if(chips == 1){
				p.sendMessage(PluginMeta.prefix + "�cDir wurde �e" + chips + " �6Chip �cabgezogen.");
			} else {
				p.sendMessage(PluginMeta.prefix + "�cDir wurden �e" + chips + " �6Chips �cabgezogen.");
			}
		} catch (Exception e){
			p.sendMessage(PluginMeta.prefix + "�c" + "Something went wrong..");
		   	p.sendMessage(PluginMeta.prefix + "�c" + "Take a look in the console!");
		  	e.printStackTrace();
		}
	}
	
	public static int getRank(String p) throws Exception {
		int rank = 0;
		
		String UUID = getUUID(p);
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + UUID + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			rank = qr.rs.getInt("rank");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return rank;
	}
	
	public static int getChips(Player p) throws Exception {
		int chips = 0;
		UUID user = p.getUniqueId();
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			chips = qr.rs.getInt("chips");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return chips;
	}
	
	public static boolean isNicked(String name){
		if(PvPHubCore.nicked.containsKey(name)){
			return true;
		} else {
			return false;
		}
	}
	
	public static void isInDatabase(Player p) throws Exception {
		String user = p.getName();
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(user) + "'");
		qr.rs.last();

		if (qr.rs.getRow() == 0)
	      {
	        System.err.println("[PvPHubCore] User not Found!");
	        HandlerStorage.getHandler("main").execute("INSERT INTO users (`id` ,`uuid` ,`name` ,`rank` ,`coins` ,`chips`, `signature`)VALUES (NULL , '" + getUUID(user) + "', '" + p.getName() + "', 0, 100, 0, '');");
	      }
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
	}
	
	public static void setDataCFG(Player p) throws Exception {
		PvPHubCore.user_cfg.set("data." + getUUID(p.getName()) + ".name", p.getName());
		PvPHubCore.user_cfg.set("data." + getUUID(p.getName()) + ".rank", getRank(p.getName()));
		PvPHubCore.user_cfg.save(PvPHubCore.file);
		p.sendMessage(PluginMeta.prefix + "�aDaten erfolgreich geladen.");
	}
	
	public static String getUUID(String player) {
		@SuppressWarnings("deprecation")
		OfflinePlayer p = Bukkit.getOfflinePlayer(player);
		return p.getUniqueId().toString().replace("-", "");
	}
	
	public static void connect(Plugin plugin, String servername, Player p) throws IOException {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(stream);
		
		out.writeUTF("Connect");
		out.writeUTF(servername);
		
		p.sendPluginMessage(plugin, "BungeeCord", stream.toByteArray());
	}
	
	public static int hasAutoNick(Player p) throws Exception {
		int l = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			l = qr.rs.getInt("setting_autoNick");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return l;
	}
	
	public static int hasShowStats(Player p) throws Exception {
		int l = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			l = qr.rs.getInt("setting_showStats");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return l;
	}
	
	public static int hasTeamSpawn(Player p) throws Exception {
		int l = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			l = qr.rs.getInt("setting_teamSpawn");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return l;
	}
	
	public static int hasFriends(Player p) throws Exception {
		int l = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			l = qr.rs.getInt("setting_allowFriendRequests");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return l;
	}
	
	public static int hasServerLoc(Player p) throws Exception {
		int l = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + getUUID(p.getName()) + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			l = qr.rs.getInt("setting_serverLocation");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return l;
	}
	
	public static void updateSetting(Player p, String setting, boolean isActive) throws Exception {
		int activate = 0;
		if(isActive == true){
			activate = 1;
		}
		HandlerStorage.getHandler("main").execute("UPDATE `users` SET `setting_" + setting + "` = " + activate + " WHERE `uuid` = '" + getUUID(p.getName()) + "' ");
	}
	
	public static void achieve(int aid, Player p) throws Exception {
		String pidstring = getPvPHubId(p) + "";
		
		if(AchievementData.hasAchieved(aid, p) == false){
			String achievers = AchievementData.getAchievers(aid);
			String achieversNew = achievers + "," + pidstring;
			
			HandlerStorage.getHandler("main").execute("UPDATE `achievements` SET `achieved_by` = '" + achieversNew + "' WHERE id=" + aid + "");
			
			if(isOnline(p.getName())){
				p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
				
				addCoins(p, 50);
				
				p.sendMessage("�a******* �k<({�r�a ACHIEVEMENT UNLOCKED �k})> �r�a*******");
				p.sendMessage("");
				p.sendMessage("    �e�l" + AchievementData.getName(aid));
				p.sendMessage("       �7" + AchievementData.getDescription(aid));
				p.sendMessage("");
				p.sendMessage("�a******* �k<({�r�a ACHIEVEMENT UNLOCKED �k})> �r�a*******");
			}
		}
	}
	
	public static void sendTitle(String mainTitle, String subTitle, Player p){
		if(is1_8(p)){
			IChatBaseComponent chatTitle = ChatSerializer.a("{\"text\": \"" + mainTitle + "\"}");
			PacketTitle title = new PacketTitle(Action.TITLE, chatTitle);
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);
			
			IChatBaseComponent sTitle = ChatSerializer.a("{\"text\": \"" + subTitle + "\"}");
			PacketTitle stitle = new PacketTitle(Action.SUBTITLE, sTitle);
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(stitle);
		}
	}
	
	public static boolean is1_8(Player p){
		boolean b = false;
		if(((CraftPlayer) p).getHandle().playerConnection.networkManager.getVersion() >= 47){
			b = true;
		}
		
		return b;
	}
	
	public static void sendModifiedTablist(String header, String footer, Player p){
		if(is1_8(p)){
			IChatBaseComponent tabTitle = ChatSerializer.a("{\"text\": \"" + header + "\"}");
            IChatBaseComponent tabFoot = null;
			try {
				tabFoot = ChatSerializer.a("{\"text\": \"" + footer + "\"}");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
         
            PacketTabHeader a = new PacketTabHeader(tabTitle, tabFoot);
         
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(a);
		}
	}
	
}
