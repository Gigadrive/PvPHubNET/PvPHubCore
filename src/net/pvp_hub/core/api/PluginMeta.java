package net.pvp_hub.core.api;

public class PluginMeta {

	// PvP-Hub
	public static final String prefix = "�4[�cPvP-Hub�4] �6";
	public static final String noplayer = prefix + "�cDu musst ein Spieler sein!";
	public static final String noperms = prefix + "�cKeine Rechte.";
	public static final String soon = prefix + "�cDieses Feature folgt bald.";
	public static final String prefixFriend = "�4[�cFreunde�4] �6";
	public static final String prefixBroadcaster = "�c[�e!�c] �6";
	
	// MotDs
	public static final String mainMotd = "�c�l�oPvP-Hub.net�r �7- �6BALD!�r\n�4HungerGames �7| �a??? �7| �9???";
	
	// StaffNotify
	public static final String sn_prefix = "�7[�cStaffNotify�7] �7";
	public static final String sn_prefix_chat = "�7[�cStaffNotify�7|�bTeam-Chat�7] �7";
	public static final String sn_prefix_report = "�7[�cStaffNotify�7|�4Report�7] �7";
	public static final String sn_prefix_bans = "�7[�cStaffNotify�7|�5Bans�7] �7";
	public static final String sn_prefix_join = "�7[�cStaffNotify�7|�a+] �7";
	public static final String sn_prefix_leave = "�7[�cStaffNotify�7|�c-] �7";
	
	
}