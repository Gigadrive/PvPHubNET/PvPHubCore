package net.pvp_hub.core.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RandomNick {

	private static Random randomGenerator;
    public static List<String> list = new LinkedList<String>();
    public static int currentLine = 0;

    
    public static String choose(File f) throws FileNotFoundException
    {
       String result = null;
       Random rand = new Random();
       int n = 0;
       for(Scanner sc = new Scanner(f); sc.hasNext(); )
       {
          ++n;
          String line = sc.nextLine();
          if(rand.nextInt(n) == 0)
             result = line;         
       }

       return result;      
    }

    public static String getRandomNick() throws Exception {
    	if(RandomNick.list.size() == 0){
    		RandomNick.list = Files.readAllLines(Paths.get("plugins/PvPHubCore/nickList.txt"), StandardCharsets.UTF_8);
			
        	Collections.shuffle(RandomNick.list);

            return RandomNick.list.get(0);
    	} else {
    		list.clear();
    		
    		RandomNick.list = Files.readAllLines(Paths.get("plugins/PvPHubCore/nickList.txt"), StandardCharsets.UTF_8);
			
        	Collections.shuffle(RandomNick.list);

            return RandomNick.list.get(0);
    	}
    }
	
}
