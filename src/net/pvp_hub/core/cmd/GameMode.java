package net.pvp_hub.core.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameMode implements CommandExecutor {

	private PvPHubCore plugin;
	public GameMode(PvPHubCore plugin){
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("gamemode")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				try {
					if(PlayerUtilities.getRank(p.getName()) >= 6){
						String usage = PluginMeta.prefix + "�c/" + label + " <0|1|2|survival|creative|adventure> [Player]";
						if(args.length == 0){
							p.sendMessage(usage);
						} else if(args.length == 1){
							if(args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("survival")){
								p.sendMessage(PluginMeta.prefix + "�aNeuer Gamemode: SURVIVAL");
								p.setGameMode(org.bukkit.GameMode.SURVIVAL);
							} else if(args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("creative")){
								p.sendMessage(PluginMeta.prefix + "�aNeuer Gamemode: CREATIVE");
								p.setGameMode(org.bukkit.GameMode.CREATIVE);
							} else if(args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("adventure")){
								p.sendMessage(PluginMeta.prefix + "�aNeuer Gamemode: ADVENTURE");
								p.setGameMode(org.bukkit.GameMode.ADVENTURE);
							} else {
								p.sendMessage(usage);
							}
						} else if(args.length == 2){
							if(PlayerUtilities.isOnline(args[1])){
								Player p2 = Bukkit.getPlayer(args[1]);
								if(args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("survival")){
									p.sendMessage(PluginMeta.prefix + "�a" + args[1] + "s neuer Gamemode: SURVIVAL");
									p2.setGameMode(org.bukkit.GameMode.SURVIVAL);
								} else if(args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("creative")){
									p.sendMessage(PluginMeta.prefix + "�a" + args[1] + "s neuer Gamemode: CREATIVE");
									p2.setGameMode(org.bukkit.GameMode.CREATIVE);
								} else if(args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("adventure")){
									p.sendMessage(PluginMeta.prefix + "�a" + args[1] + "s neuer Gamemode: ADVENTURE");
									p2.setGameMode(org.bukkit.GameMode.ADVENTURE);
								} else {
									p.sendMessage(usage);
								}
							} else {
								p.sendMessage(PluginMeta.prefix + "�c" + args[1] + " ist nicht auf diesem Server online.");
							}
						}
					} else {
						p.sendMessage(PluginMeta.noperms);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
