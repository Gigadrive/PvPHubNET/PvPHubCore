package net.pvp_hub.core.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.achievement.AchievementData;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GiveAchievement implements CommandExecutor {

	private PvPHubCore plugin;
	public GiveAchievement(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("giveachievement")){
			if(sender.isOp()){
				if(args.length == 3){
					if(PlayerUtilities.isOnline(args[0])){
						Player p = Bukkit.getServer().getPlayer(args[0]);
						int aid = Integer.parseInt(args[1]);
						int telToSpawn = Integer.parseInt(args[2]);
						
						try {
							PlayerUtilities.achieve(aid, p);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						if(telToSpawn == 1){
							Location spawn = new Location(Bukkit.getWorld("world"), 188.5376D, 61.0D, -328.4983D, -1.1842F, 17.5007F);
							p.teleport(spawn);
						}
						
						try {
							sender.sendMessage(PluginMeta.prefix + "�aDer Spieler " + args[0] + " hat das Achievement " + AchievementData.getName(aid) + " erhalten.");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						sender.sendMessage(PluginMeta.prefix + "�c" + args[0] + " ist nicht online.");
					}
				} else {
					sender.sendMessage(PluginMeta.prefix + "�c/giveachievement <Player> <Achievement-ID> <0|1>");
				}
			} else {
				sender.sendMessage(PluginMeta.noperms);
			}
		}
		
		return false;
	}
	
}
