package net.pvp_hub.core.cmd;

import net.pvp_hub.core.PvPHubCore;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Money implements CommandExecutor {

	private PvPHubCore plugin;
	public Money(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("money")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				p.performCommand("coins");
				p.performCommand("chips");
			}
		}
		
		return false;
	}
	
}
