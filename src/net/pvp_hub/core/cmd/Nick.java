package net.pvp_hub.core.cmd;

import java.util.Map.Entry;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.core.player.Rank;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.kitteh.tag.TagAPI;

public class Nick implements CommandExecutor {

	private PvPHubCore plugin;
	public Nick(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("nick")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				
				try {
					if(PlayerUtilities.getRank(p.getName()) >= 3){
						if(args.length == 0){
							p.sendMessage(PluginMeta.prefix + "�c/nick <Name>");
						} else {
							if(args[0].equalsIgnoreCase("off") || args[0].equalsIgnoreCase(p.getName())){
								PvPHubCore.nicked.remove(p.getName());
								try {
									int rank = PlayerUtilities.getRank(p.getName());
									p.setDisplayName(PlayerUtilities.getRankColor(rank) + p.getName());
								} catch (Exception e1) {
									e1.printStackTrace();
								}
								p.setPlayerListName(PlayerUtilities.cutDisplayName(p.getDisplayName()));
								PlayerUtilities.setDataCFG(p);
								TagAPI.refreshPlayer(p);
								p.sendMessage(PluginMeta.prefix + "�aDein Nickname wurde zur�ckgesetzt.");
							} else if(args[0].equalsIgnoreCase("random")){
								p.performCommand("rnick");
							} else {
								if(PlayerUtilities.getRank(args[0]) == 0){
									if(PlayerUtilities.isOnline(args[0])){
										p.sendMessage(PluginMeta.prefix + "�cDer angegebene Spieler ist auf deinem Server.");
									} else {
										p.setDisplayName("�7" + args[0]);
										p.setPlayerListName(PlayerUtilities.cutDisplayName(p.getDisplayName()));
										PvPHubCore.nicked.put(p.getName(), args[0]);
										TagAPI.refreshPlayer(p);
										p.sendMessage(PluginMeta.prefix + "�aDu hei�t nun " + p.getDisplayName());
										p.sendMessage(PluginMeta.prefix + "�4Dieser Befehl wird aufgezeichnet!");
										p.sendMessage(PluginMeta.prefix + "�4Missbrauch f�hrt zu Rangverlust!");
										PvPHubCore.createNickLog(p.getName(), args[0]);
									}
								} else {
									p.sendMessage(PluginMeta.prefix + "�cDer angegebene Spieler hat einen zu hohen Rang.");
								}
							}
						}
					} else {
						p.sendMessage(PluginMeta.noperms);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("realname")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				
				try {
					if(PlayerUtilities.getRank(p.getName()) > 6){
						if(args.length == 0){
							p.sendMessage(PluginMeta.prefix + "�c/realname <Name>");
						} else {
							String realname = "";
							if(PvPHubCore.nicked.containsValue(args[0])){
								for (Entry<String, String> entry : PvPHubCore.nicked.entrySet()) {
						            if (entry.getValue().equals(args[0])) {
						                realname = entry.getKey();
						            }
						        }
								p.sendMessage(PluginMeta.prefix + "�7" + args[0] + "�as richtiger Name ist " + realname);
							} else {
								p.sendMessage(PluginMeta.prefix + "�c" + args[0] + "�c ist nicht genickt.");
							}
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
