package net.pvp_hub.core.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Plugins implements CommandExecutor {

	private PvPHubCore plugin;
	public Plugins(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("plugins")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				
				if(p.isOp()){
					p.performCommand("bukkit:plugins");
				} else {
					p.sendMessage(PluginMeta.noperms);
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
