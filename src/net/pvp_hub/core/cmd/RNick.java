package net.pvp_hub.core.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.core.api.RandomNick;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RNick implements CommandExecutor {

	private PvPHubCore plugin;
	public RNick(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("rnick")){
			Player p = (Player)sender;
			try {
				p.performCommand("nick " + RandomNick.getRandomNick());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
}
