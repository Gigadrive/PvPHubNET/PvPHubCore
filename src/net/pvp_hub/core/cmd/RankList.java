package net.pvp_hub.core.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RankList implements CommandExecutor {

	private PvPHubCore plugin;
	public RankList(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("ranklist")){
			if(sender.isOp()){
				sender.sendMessage(PluginMeta.prefix + "�6=== �eRang-IDs �6===");
				sender.sendMessage(PluginMeta.prefix + "�c11 �8- �cOWNER");
				sender.sendMessage(PluginMeta.prefix + "�c10 �8- �cADMIN");
				sender.sendMessage(PluginMeta.prefix + "�99 �8- �9DEVELOPER");
				sender.sendMessage(PluginMeta.prefix + "�e8 �8- �eMODERATOR");
				sender.sendMessage(PluginMeta.prefix + "�a7 �8- �aTEAM");
				sender.sendMessage(PluginMeta.prefix + "�36 �8- �3BAU-TEAM");
				sender.sendMessage(PluginMeta.prefix + "�b5 �8- �bBETA");
				sender.sendMessage(PluginMeta.prefix + "�54 �8- �5YOUTUBER");
				sender.sendMessage(PluginMeta.prefix + "�53 �8- �5VIP");
				sender.sendMessage(PluginMeta.prefix + "�62 �8- �6LIFE-TIME-PREMIUM");
				sender.sendMessage(PluginMeta.prefix + "�61 �8- �6PREMIUM");
				sender.sendMessage(PluginMeta.prefix + "�70 �8- �7USER");
			} else {
				sender.sendMessage(PluginMeta.noperms);
			}
		}
		
		return false;
	}
	
}
