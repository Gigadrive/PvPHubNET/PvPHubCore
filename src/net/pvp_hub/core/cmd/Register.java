package net.pvp_hub.core.cmd;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.minecraft.util.org.apache.commons.codec.binary.Hex;
import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Register implements CommandExecutor {

	private PvPHubCore plugin;
	public Register(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("register")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				
				p.sendMessage(p.getUniqueId().toString().replace("-", ""));
				
				if(args.length != 2){
					p.sendMessage(PluginMeta.prefix + "�c/register <Passwort> <Passwort wdh.>");
				} else {
					if(args[0].equals(args[1])){
						String string = args[0];
						MessageDigest messageDigest = null;
						try {
							messageDigest = MessageDigest.getInstance("MD5");
						} catch (NoSuchAlgorithmException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						messageDigest.reset();
						messageDigest.update(string.getBytes(Charset.forName("UTF8")));
						final byte[] resultByte = messageDigest.digest();
						final String result = new String(Hex.encodeHex(resultByte));
						
						PlayerUtilities.updatePassword(p, result);
						
						p.sendMessage(PluginMeta.prefix + "�aDein Passwort wurde ge�ndert.");
						p.sendMessage(PluginMeta.prefix + "�aDu kannst dich nun damit auf der Website einloggen.");
					} else {
						p.sendMessage(PluginMeta.prefix + "�cDie beiden Passw�rter stimmen nicht �berein.");
					}
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
