package net.pvp_hub.core.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.core.player.Rank;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetRank implements CommandExecutor {

	private PvPHubCore plugin;
	public SetRank(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("rank")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				try {
					if(PlayerUtilities.getRank(p.getName()) > 8){
						if(args.length == 0){
							p.sendMessage(PluginMeta.prefix + "�c/rank <set|list> [Player] [Rank-ID]");
						} else if(args[0].equalsIgnoreCase("list")){
							p.performCommand("ranklist");
						} else if(args.length == 3 && args[0].equalsIgnoreCase("set")){
							String pname = args[1];
							int rankid = Integer.parseInt(args[2]);
							
							PlayerUtilities.setRank(pname, rankid);
							p.sendMessage(PluginMeta.prefix + "�aDer Rang wurde geupdated!");
						}
					} else {
						sender.sendMessage(PluginMeta.noperms);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
