package net.pvp_hub.core.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class Skull implements CommandExecutor {

	private PvPHubCore plugin;
	public Skull(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("skull")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				
				try {
					if(!(PlayerUtilities.getRank(p.getName()) < 6)){
						if(args.length != 0){
							ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
						    SkullMeta meta = (SkullMeta)skull.getItemMeta();
						    meta.setOwner(args[0]);
						    skull.setItemMeta(meta);
						    p.getInventory().addItem(new ItemStack[] { skull });
						    p.sendMessage(PluginMeta.prefix + "�aDu hast nun '" + args[0] + "'s Kopf.");
						} else {
							p.sendMessage(PluginMeta.prefix + "/skull <Player-Name>");
						}
					} else {
						p.sendMessage(PluginMeta.noperms);
					}
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
