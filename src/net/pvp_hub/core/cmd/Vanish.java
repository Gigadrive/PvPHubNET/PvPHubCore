package net.pvp_hub.core.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Vanish implements CommandExecutor {

	private PvPHubCore plugin;
	public Vanish(PvPHubCore plugin){
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("vanish")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				try {
					if(PlayerUtilities.getRank(p.getName()) >= 7){
						if(PvPHubCore.vanished.contains(p)){
							for(Player all : Bukkit.getOnlinePlayers()){
								if(!all.isOp()){
									all.showPlayer(p);
								}
							}
							PvPHubCore.vanished.remove(p);
							p.sendMessage(PluginMeta.prefix + "�aDu wurdest entvanishet!");
							p.sendMessage(PluginMeta.prefix + "�aNormale Spieler k�nnen dich wieder sehen.");
						} else {
							for(Player all : Bukkit.getOnlinePlayers()){
								if(!all.isOp()){
									all.hidePlayer(p);
								}
							}
							PvPHubCore.vanished.add(p);
							p.sendMessage(PluginMeta.prefix + "�aDu wurdest gevanishet!");
							p.sendMessage(PluginMeta.prefix + "�aNormale Spieler k�nnen dich nicht mehr sehen.");
						}
					} else {
						p.sendMessage(PluginMeta.noperms);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
