package net.pvp_hub.core.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerLocaleChangeEvent extends PlayerEvent {
	
	/*
	 * Thanks to Overcast Network for their contribution!
	 * Especially Isaac Moore (@iamramsey)
	 * 
	 * https://github.com/OvercastNetwork/SportBukkit/blob/9126a97964b98b16a9e923e4a3cecc66e4f22877/Bukkit/0029-Add-PlayerLocaleChangeEvent.patch
	 */
	
	private static final HandlerList handlers = new HandlerList();
	private final String oldLocale;
	private final String newLocale;
	
	public PlayerLocaleChangeEvent(final Player player, final String oldLocale, final String newLocale){
		super(player);
		this.oldLocale = oldLocale;
		this.newLocale = newLocale;
	}
	
	public String getOldLocale(){
		return oldLocale;
	}
	
	public String getNewLocale(){
		return newLocale;
	}
	
	@Override
	public HandlerList getHandlers(){
		return handlers;
	}
	
	public static HandlerList getHandlerList(){
		return handlers;
	}

}
