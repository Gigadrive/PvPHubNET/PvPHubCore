package net.pvp_hub.core.language;

import net.pvp_hub.core.language.translation.TranslationDE;
import net.pvp_hub.core.language.translation.TranslationEN;

public enum Language {

	GERMAN(0), ENGLISH(1);
	
	private int id;
	
	Language(int id){
		this.id = id;
	}
	
	public int getID() {
		return this.id;
	}
	
	public static Language fromID(int id) {
		switch(id) {
			case 0: return Language.GERMAN;
			case 1: return Language.ENGLISH;
		}
		return Language.GERMAN;
	}
	
	public static Language fromName(String name){
		switch(name) {
			case "de_DE": return Language.GERMAN;
			case "en_US": return Language.ENGLISH;
			case "en_GB": return Language.ENGLISH;
		}
		return Language.ENGLISH;
	}
	
	public static Translation getTranslation(Language lang) {
		switch(lang) {
			case GERMAN: return new TranslationDE();
			case ENGLISH: return new TranslationEN();
		}
		return new TranslationDE();
	}
	
}