package net.pvp_hub.core.language;

import java.util.ArrayList;

import org.bukkit.entity.Player;

public interface Translation {

	public int getID();
	
	public String joinServer(Player player);
	public String quitLobby(Player player);
	public String goToSoon(String color);
	public String teleportedTo(String gameType);
	
	public String loadSuccess();
	public String gameModes();
	public String settings();
	public String hidePlayers();
	public String spawn();
	public ArrayList<String> gameModesLore();
	public ArrayList<String> settingsLore();
	public ArrayList<String> hidePlayersLore();
	public ArrayList<String> spawnLore();
	public String goToSurvivalGames();
	public String goToInfWars();
	public String goToDeathmatch();
	public String goToGunGame();
	public String goToSPVP();
	public String goToBlockJump();
	public String goToRaceBow();
	
}
