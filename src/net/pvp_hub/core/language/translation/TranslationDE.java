package net.pvp_hub.core.language.translation;

import java.util.ArrayList;

import net.pvp_hub.core.language.Language;
import net.pvp_hub.core.language.Translation;

import org.bukkit.entity.Player;

public class TranslationDE implements Translation {

	public int getID(){
		return Language.GERMAN.getID();
	}

	@Override
	public String joinServer(Player player) {
		String s = null;
		try {
			s = player.getDisplayName() + " �6hat den Server betreten."; 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return s;
	}

	@Override
	public String quitLobby(Player player) {
		String s = null;
		try {
			s = player.getDisplayName() + " �6hat den Server verlassen.";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return s;
	}

	@Override
	public String gameModes() {
		return "Spielmodi";
	}

	@Override
	public String goToSurvivalGames() {
		return "�7Zu den �4Survival Games �7Servern.";
	}

	@Override
	public String goToInfWars() {
		return "�7Zu den �2Infection Wars �7Servern.";
	}

	@Override
	public String goToDeathmatch() {
		return "�7Zu den �3Deathmatch �7Servern.";
	}

	@Override
	public String goToSoon(String color) {
		return "�7Zu den " + color + "??? �7Servern.";
	}

	@Override
	public String teleportedTo(String gameType) {
		return "�aDu wurdest zu " + gameType + " �ateleportiert.";
	}

	@Override
	public String loadSuccess() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String settings() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String hidePlayers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String spawn() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> gameModesLore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> settingsLore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> hidePlayersLore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> spawnLore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String goToGunGame() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String goToSPVP() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String goToBlockJump() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String goToRaceBow() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
