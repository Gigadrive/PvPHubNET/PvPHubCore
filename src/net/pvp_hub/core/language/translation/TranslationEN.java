package net.pvp_hub.core.language.translation;

import java.util.ArrayList;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.language.Language;
import net.pvp_hub.core.language.Translation;

import org.bukkit.entity.Player;

public class TranslationEN implements Translation {

	public int getID(){
		return Language.GERMAN.getID();
	}

	@Override
	public String joinServer(Player player) {
		String s = null;
		try {
			s = player.getDisplayName() + " �6has joined the server."; 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return s;
	}

	@Override
	public String quitLobby(Player player) {
		String s = null;
		try {
			s = player.getDisplayName() + " �6has left the server.";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return s;
	}

	@Override
	public String gameModes() {
		return "Gamemodes";
	}

	@Override
	public String goToSurvivalGames() {
		return "�7Go to �4Survival Games�7.";
	}

	@Override
	public String goToInfWars() {
		return "�7Go to �2Infection Wars�7.";
	}

	@Override
	public String goToDeathmatch() {
		return "�7Go to �3Deathmatch�7.";
	}

	@Override
	public String goToSoon(String color) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String teleportedTo(String gameType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String loadSuccess() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String settings() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String hidePlayers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String spawn() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> gameModesLore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> settingsLore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> hidePlayersLore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> spawnLore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String goToGunGame() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String goToSPVP() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String goToBlockJump() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String goToRaceBow() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
