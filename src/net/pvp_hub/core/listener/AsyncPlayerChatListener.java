package net.pvp_hub.core.listener;

import java.util.IllegalFormatException;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.player.Rank;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {

	private PvPHubCore plugin;
	public AsyncPlayerChatListener(PvPHubCore plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		
		if(!PlayerUtilities.isNicked(p.getName())){
			try {
				if(PlayerUtilities.getRank(p.getName()) == 0){
					e.setFormat("�8<�r" + p.getDisplayName() + "�8>�r " + e.getMessage());
				} else {
					int r = PlayerUtilities.getRank(p.getName());
					if(r == 1){
						e.setFormat("�8<[�7Premium�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 2){
						e.setFormat("�8<[�7Premium�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 3){
						e.setFormat("�8<[�7VIP�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 4){
						e.setFormat("�8<[�7YouTuber�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 5){
						e.setFormat("�8<[�7Beta-Tester�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 6){
						e.setFormat("�8<[�7Bau-Team�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 7){
						e.setFormat("�8<[�7Team�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 8){
						e.setFormat("�8<[�7Moderator�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 9){
						e.setFormat("�8<[�7Dev�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 10){
						e.setFormat("�8<[�7Admin�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else if(r == 11){
						e.setFormat("�8<[�7Owner�8] �r" + p.getDisplayName() + "�8> �r" + e.getMessage());
					} else {
						e.setFormat("�8<�r" + p.getDisplayName() + "�8>�r " + e.getMessage());
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			e.setFormat("�8<�r" + p.getDisplayName() + "�8>�r " + e.getMessage());
		}
	}
	
}
