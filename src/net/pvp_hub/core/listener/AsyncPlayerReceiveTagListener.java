package net.pvp_hub.core.listener;

import net.pvp_hub.core.PvPHubCore;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;

public class AsyncPlayerReceiveTagListener implements Listener {

	private PvPHubCore plugin;
	public AsyncPlayerReceiveTagListener(PvPHubCore plugin){
		this.plugin = plugin;
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onReceiveNameTag(AsyncPlayerReceiveNameTagEvent e){
		if(PvPHubCore.nicked.containsKey(e.getNamedPlayer().getName())){
			e.setTag(PvPHubCore.nicked.get(e.getNamedPlayer().getName()));
		} else {
			e.setTag(e.getNamedPlayer().getName());
		}
	}
	
}
