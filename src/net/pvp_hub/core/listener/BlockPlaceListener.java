package net.pvp_hub.core.listener;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {

	private PvPHubCore plugin;
	public BlockPlaceListener(PvPHubCore plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		Player p = e.getPlayer();
		if(e.getBlock().getType().equals(Material.COMMAND)){
			p.sendMessage(PluginMeta.prefix + "�cCommand-Blocks und ihre Befehle werden aufgezeichnet!");
			int x = e.getBlock().getX();
			int y = e.getBlock().getY();
			int z = e.getBlock().getZ();
			
			try {
				PvPHubCore.createCmdLog(p.getName(), x, y, z);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
}
