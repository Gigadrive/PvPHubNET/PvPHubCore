package net.pvp_hub.core.listener;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

	private PvPHubCore plugin;
	public PlayerJoinListener(PvPHubCore plugin){
		this.plugin = plugin;
	}
	
	@SuppressWarnings({ "static-access", "deprecation" })
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		
		try {
			PlayerUtilities.isInDatabase(p);
			PlayerUtilities.setDataCFG(p);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(PlayerUtilities.is1_8(p)){
			try {
				PlayerUtilities.sendModifiedTablist("�c�lPvP-Hub.net �8| �6" + PlayerUtilities.getServer(p).toUpperCase() + "\n", "\n�bTeamSpeak3: �ePvP-Hub.net\n�bWebsite: �ePvP-Hub.net", p);
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}
		
		try {
			String col = "�7";
			int r = PlayerUtilities.getRank(p.getName());
			
			if(r == 0){
				col = "�7";
				PvPHubCore.user.addPlayer(p);
			} else if(r == 1){
				col = "�6";
				PvPHubCore.premium.addPlayer(p);
			} else if(r == 2){
				col = "�6";
				PvPHubCore.premium.addPlayer(p);
			} else if(r == 3){
				col = "�5";
				PvPHubCore.vip.addPlayer(p);
			} else if(r == 4){
				col = "�5";
				PvPHubCore.vip.addPlayer(p);
			} else if(r == 5){
				col = "�b";
				PvPHubCore.beta.addPlayer(p);
			} else if(r == 6){
				col = "�3";
				PvPHubCore.build.addPlayer(p);
			} else if(r == 7){
				col = "�a";
				PvPHubCore.team.addPlayer(p);
			} else if(r == 8){
				col = "�e";
				PvPHubCore.mod.addPlayer(p);
			} else if(r == 9){
				col = "�9";
				PvPHubCore.dev.addPlayer(p);
			} else if(r == 10){
				col = "�c";
				PvPHubCore.admin.addPlayer(p);
			} else if(r == 11){
				col = "�c";
				PvPHubCore.owner.addPlayer(p);
			}
			
			p.setDisplayName(col + p.getName());
			p.setPlayerListName(col + PlayerUtilities.cutDisplayName(p.getName()));
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}
	
}
