package net.pvp_hub.core.listener;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.language.Language;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

	private PvPHubCore plugin;
	public PlayerQuitListener(PvPHubCore plguin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		
		if(PvPHubCore.vanished.contains(p)){
			PvPHubCore.vanished.remove(p);
		}
		
		if(PlayerUtilities.isNicked(p.getName())){
			PvPHubCore.nicked.remove(p.getName());
		}
		
		PvPHubCore.leaveTeam(p);
		
	}
	
}
