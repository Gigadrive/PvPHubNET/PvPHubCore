package net.pvp_hub.core.listener;

import net.pvp_hub.core.PvPHubCore;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerPingListener implements Listener {

	private PvPHubCore plugin;
	public ServerPingListener(PvPHubCore plugin){
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPing(ServerListPingEvent e){
		e.setMotd(plugin.getGameState().getName());
	}
	
}
