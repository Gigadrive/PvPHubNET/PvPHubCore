package net.pvp_hub.core.listener;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class TabCompleteListener implements Listener {

	private PvPHubCore plugin;
	public TabCompleteListener(PvPHubCore plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onComplete(PlayerCommandPreprocessEvent e){
		Player p = e.getPlayer();
		
		if(!p.isOp()){
			e.setCancelled(true);
		}
	}
	
}