package net.pvp_hub.core.player;

public enum Rank {

	OWNER(11, "Owner", "�c"),
	ADMIN(10, "Admin", "�c"),
	DEVELOPER(9, "Developer", "�9"),
	MODERATOR(8, "Moderator", "�e"),
	TEAM(7, "Server-Team", "�a"),
	BAUTEAM(6, "Bau-Team", "�3"),
	BETA(5, "Beta", "�b"),
	YOUTUBER(4, "YouTuber", "�5"),
	VIP(3, "VIP", "�5"),
	LIFETIMEPREMIUM(2, "Premium", "�6"),
	PREMIUM(1, "Premium", "�6"),
	USER(0, "User", "�7");
	
	private int id;
	private String name;
	private String color;
	
	Rank(int id, String name, String color) {
		this.id = id;
		this.name = name;
		this.color = color;
	}
	
	public int getID() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public static Rank fromID(int id) {
		switch(id) {
			case 0: return USER;
			case 1: return PREMIUM;
			case 2: return VIP;
			case 3: return YOUTUBER;
			case 4: return BETA;
			case 5: return BAUTEAM;
			case 6: return TEAM;
			case 7: return MODERATOR;
			case 8: return DEVELOPER;
			case 9: return ADMIN;
		}
		return USER;
	}
	
	public static Rank getMaxRank() {
		return Rank.OWNER;
	}
	
	public static Rank getMinRank() {
		return Rank.USER;
	}
	
}
