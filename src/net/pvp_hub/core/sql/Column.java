package net.pvp_hub.core.sql;

public enum Column {
	
	ID("id"), UUID("uuid"), NAME("name"), RANK("rank"), LANGUAGE("setting_language"), COINS("coins"), CHIPS("chips"), FACEBOOK("fb"), YOUTUBE("yt"), TWITTER("tw"), FRIENDS("friends"), ACCEPT_FRIENDS("accept_friends"), WEBSITE_PASS("website_pass"), FIRSTJOIN("firstjoin"), IS_ONLINE("is_online"), LAST_SERVER("server"), SETTING_AUTONICK("setting_autoNick"), SETTING_SHOWSTATS("setting_showStats"), SETTING_ALLOWFRIENDS("setting_allowFriendRequests"), SETTING_SHOWSERVER("setting_serverLocation");
	
	private String name;
	
	private Column(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
