package net.pvp_hub.core.sql;

import java.sql.SQLException;


public class ConnectionHandler {
	
	private static MySQLHandler handler;
	
	private static String table = null;
	
	public static void initHandler(String host, int port, String user, String password, String database) throws ClassNotFoundException, SQLException {
		handler = new MySQLHandler();
		handler.setOptions(host, port, user, password, database);
		handler.openConnection();
	}
	
	public static void initHandler(String host, int port, String user, String password, String database, String table) throws ClassNotFoundException, SQLException {
		handler = new MySQLHandler();
		handler.setOptions(host, port, user, password, database);
		ConnectionHandler.table = table;
		handler.openConnection();
	}
	
	public static MySQLHandler getHandler() {
		return handler;
	}
	
	public static String getTable() {
		return table;
	}

}
