package net.pvp_hub.core.sql;

import tk.theakio.sql.MySQLData;

/**
 * TODO MUST BE IN A CONFIG FILE
 *
 */
public class SQLData {

	public static final String hostname = MySQLData.mysql_u_host;
	public static final int port = MySQLData.mysql_u_port;
	public static final String username = MySQLData.mysql_u_user;
	public static final String password = MySQLData.mysql_u_pass; //PROTECTED
	public static final String database = MySQLData.mysql_u_database;
	
	public static final String usertable = "users";
	
	public static final String authkey = "iGnV8HH3OZgnrBO7G1LcpVePIF6YC2tkJGovR2IOaNwDOOA53D5FJIDebdig1Gh33KjywqFKZRUvLHLOj6eT36g0qEeEykoVIkZcTwVGMaTUTqPggop1Rjfvzs5b807O";
	public static final String url = "https://www.pvp-hub.net/assets/api/register.api.php";
	
}
