package net.pvp_hub.core.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface SQLHandler {
	
	boolean openConnection() throws ClassNotFoundException, SQLException;
	Connection getConnection();
	boolean hasConnection() throws SQLException;
	boolean execute(String statement) throws ClassNotFoundException, SQLException;
	int executeUpdate(String statement) throws ClassNotFoundException, SQLException;
	QueryResult executeQuery(String statement) throws ClassNotFoundException, SQLException;
	void closeResources(ResultSet rs, PreparedStatement st) throws SQLException;
	void closeResources(QueryResult qr) throws SQLException;
	void closeConnection() throws SQLException;
	void doMessageOutput(boolean message);
	void doErrorOutput(boolean error);
	
}
