package net.pvp_hub.core.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLiteHandler implements SQLHandler {
	
	private String path;
	
	private boolean outputmsg = false;
	private boolean outputerr = true;
	private Connection con;
	
	private void outputMessage(String msg, boolean error) {
		if(outputmsg == true && error == false) {
			System.out.println("[MySQLHandler] " + msg);
		}
		else if(outputerr == true && error == true) {
			System.err.println("[MySQLHandler] " + msg);
		}
	}	
	
	@Override
	public boolean openConnection() throws ClassNotFoundException, SQLException {
		outputMessage("Connecting to Database at '" + path + "'...", false);
		Class.forName("org.sqlite.JDBC");
		con = DriverManager.getConnection("jdbc:sqlite:" + path);
		outputMessage("Connected!", false);
		return true;
	}

	@Override
	public Connection getConnection() {
		return this.con;
	}
	
	public String getPath() {
		return this.path;
	}
	
	@Override
	public boolean hasConnection() throws SQLException {
		if(this.con != null) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@Override
	public void doMessageOutput(boolean b) {
		outputmsg = b;
	}
	
	@Override
	public void doErrorOutput(boolean b) {
		outputerr = b;
	}
	
	@Override
	public boolean execute(String statement) throws ClassNotFoundException, SQLException {
		if(!hasConnection()) {
			if(!openConnection()) {
				outputMessage("Connection lost! Can't reconnect. Abroting Statement Execution!", true);
				return false;
			}
		}
		Connection con = this.con;
		PreparedStatement st = null;
		st = con.prepareStatement(statement);
		st.execute();
		this.closeResources(null, st);
		return true;
	}
	
	@Override
	public int executeUpdate(String statement) throws ClassNotFoundException, SQLException {
		if(!hasConnection()) {
			if(!openConnection()) {
				outputMessage("Connection lost! Can't reconnect. Abroting Statement Execution!", true);
				return 0;
			}
		}
		Connection con = this.con;
		PreparedStatement st = null;
		int u = 0;
		st = con.prepareStatement(statement);
		u = st.executeUpdate();
		this.closeResources(null, st);
		return u;
	}
	
	@Override
	public QueryResult executeQuery(String statement) throws ClassNotFoundException, SQLException {
		if(!hasConnection()) {
			if(!openConnection()) {
				outputMessage("Connection lost! Can't reconnect. Abroting Statement Execution!", true);
				return null;
			}
		}
		Connection con = this.con;
		PreparedStatement st = null;
		ResultSet rs = null;
		st = con.prepareStatement(statement);
		rs = st.executeQuery();
		return new QueryResult(st, rs);
	}
	
	@Override
	public void closeResources(ResultSet rs, PreparedStatement st) throws SQLException {
		if(rs != null) {
			rs.close();
		}
		if(st != null) {
			st.close();
		}
	}
	
	@Override
	public void closeResources(QueryResult qr) throws SQLException {
		closeResources(qr.rs, qr.st);
	}
	
	@Override
	public void closeConnection() throws SQLException {
		if(con != null) {
			this.con.close();
			outputMessage("Connection closed!", false);
		}
		this.con = null;
	}
	
	public void setOptions(String path) {
		this.path = path;
	}
}
