package tk.theakio.sql;

public class MySQLStatementExecutorThread extends Thread {

	private String statement;
	private SQLHandler handler;

	public MySQLStatementExecutorThread(String statement, SQLHandler handler) {
		this.statement = statement;
		this.handler = handler;
	}
	
	@Override
	public void run() {
		try {
			handler.execute(statement);
			this.interrupt();
		}
		catch(Exception e) {
			System.err.println("Failed with MySQL");
			System.err.println(e.getMessage());
		}
	}
	
	
	
}
