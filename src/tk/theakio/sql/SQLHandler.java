package tk.theakio.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLHandler {
	
	//For MYSQL
	private String host;
	private int port;
	private String user;
	private String password;
	private String database;
	
	//For SQLITE
	private String path;
	
	//For ALL
	private boolean outputmsg = false;
	private boolean outputerr = true;
	private int dbtype;
	private Connection con;
	
	//For USER
	public static final int DBTYPE_MYSQL = 0;
	public static final int DBTYPE_SQLITE = 1;
	
	private void outputMessage(String msg, boolean error) {
		if(outputmsg == true && error == false) {
			System.out.println("[MySQLHandler] " + msg);
		}
		else if(outputerr == true && error == true) {
			System.err.println("[MySQLHandler] " + msg);
		}
	}	
	

	public boolean openConnection() throws ClassNotFoundException, SQLException, InvalidDatabaseTypeException {
		if(dbtype == 0) {
			outputMessage("Connecting to Database at '" + host + ":" + port + "/" + database + "'...", false);
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password);
			outputMessage("Connected!", false);
			this.con = con;
			return true;
		}
		else if(dbtype == 1) {
			outputMessage("Connecting to Database at '" + path + "'...", false);
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:" + path);
			outputMessage("Connected!", false);
			return true;
		}
		else {
			throw new InvalidDatabaseTypeException("'" + dbtype + "' is not a valid Database Type");
		}
	}

	public Connection getConnection() {
		return this.con;
	}
	
	public String getHost() throws InvalidDatabaseTypeException {
		if(dbtype == DBTYPE_MYSQL) {
			return this.host;
		}
		else if(dbtype == DBTYPE_SQLITE) {
			return this.path;
		}
		else {
			throw new InvalidDatabaseTypeException("'" + dbtype + "' is not a valid Database Type");
		}
	}
	

	public boolean hasConnection() throws SQLException {
		if(this.con != null) {
			if(this.dbtype != DBTYPE_SQLITE) {
				if(!this.con.isClosed()) {
					if(this.con.isValid(1)) {
						return true;
					}
					else {
						return false;
					}
				}
				else {
					return false;
				}
			}
			else {
				return true;
			}
		}
		else {
			return false;
		}
	}
	
	public void doMessageOutput(boolean b) {
		outputmsg = b;
	}
	
	public void doErrorOutput(boolean b) {
		outputerr = b;
	}
	
	public void setDatabaseType(int type) throws InvalidDatabaseTypeException {
		if(type == 0) {
			dbtype = 0;
		}
		else if(type == 1) {
			dbtype = 1;
		}
		else {
			throw new InvalidDatabaseTypeException("'" + dbtype + "' is not a valid Database Type");
		}
	}
	
	public boolean execute(String statement) throws ClassNotFoundException, SQLException, InvalidDatabaseTypeException {
		if(!hasConnection()) {
			System.out.println("NOCONNECTION");
			if(!openConnection()) {
				outputMessage("Connection lost! Can't reconnect. Abroting Statement Execution!", true);
				return false;
			}
			System.out.println("RECONNECTED");
		}
		Connection con = this.con;
		PreparedStatement st = null;
		st = con.prepareStatement(statement);
		st.execute();
		this.closeRecources(null, st);
		return true;
	}
	
	public int executeUpdate(String statement) throws InvalidDatabaseTypeException, ClassNotFoundException, SQLException {
		if(!hasConnection()) {
			System.out.println("NOCONNECTION");
			if(!openConnection()) {
				outputMessage("Connection lost! Can't reconnect. Abroting Statement Execution!", true);
				return 0;
			}
			System.out.println("RECONNECTED");
		}
		Connection con = this.con;
		PreparedStatement st = null;
		int u = 0;
		st = con.prepareStatement(statement);
		u = st.executeUpdate();
		this.closeRecources(null, st);
		return u;
	}
	
	public QueryResult executeQuery(String statement) throws ClassNotFoundException, SQLException, InvalidDatabaseTypeException {
		if(!hasConnection()) {
			System.out.println("NOCONNECTION");
			if(!openConnection()) {
				outputMessage("Connection lost! Can't reconnect. Abroting Statement Execution!", true);
				return null;
			}
			System.out.println("RECONNECTED");
		}
		Connection con = this.con;
		PreparedStatement st = null;
		ResultSet rs = null;
		st = con.prepareStatement(statement);
		rs = st.executeQuery();
		return new QueryResult(st, rs);
	}
	
	public void closeRecources(ResultSet rs, PreparedStatement st) throws SQLException {
		if(rs != null) {
			rs.close();
		}
		if(st != null) {
			st.close();
		}
	}
	
	public void closeConnection() throws SQLException {
		if(con != null) {
			this.con.close();
			outputMessage("Connection closed!", false);
		}
		this.con = null;
	}
	
	public void setMySQLOptions(String host, int port, String user, String password, String database) {
		this.host = host;
		this.port = port;
		this.user = user;
		this.password = password;
		this.database = database;
	}
	
	public void setSQLiteOptions(String path) {
		this.path = path;
	}
}
